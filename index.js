const express = require('express');

const app = express();
app.use(express.json());
const path = require('path');


const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://okava:FBrrCD4dKZA8snya@cluster0-dx1gh.mongodb.net/test?retryWrites=true&w=majority";
var options = {
    useNewUrlParser: true
};
var db;
var okavaClient;

mongoConnect = () => {
    if (!db || !db.isConnected()) {
        const client = new MongoClient(uri, options);
        client.connect(function (err, _db) {
            if (err)
                return;
            db = _db;
            okavaClient = _db.db('okava');
        });
    }
}

mongoConnect();


app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

exports.getDraftsById = function (id, callback) {
    mongoConnect();
    okavaClient.collection('drafts').find(id ? {_id: id} : {}).toArray(function (err, result) {
        if (err)
            callback(err, null);

        if (result)
            callback(null, result);
    });
};

exports.setDraftsById = function (body, id, callback) {
    mongoConnect();
    okavaClient.collection('drafts').update({_id: id}, body, {upsert: true}, function (err, result) {
            if (err)
                callback(err, null);

            if (result)
                callback(null, result);
        }
    );
};

app.get('/api/drafts', function (req, res) {
    exports.getDraftsById(req.query.id, function (err, result) {
        if (result) {
            res.jsonp(result);
        }
    });
});

app.post('/api/drafts', function (req, res) {
    exports.setDraftsById(req.body, req.query.id, function (err, result) {
        if (result) {
            res.jsonp(result);
        }
    });
});

app.use(express.static(__dirname + '/public'));
app.use('/static', express.static(__dirname + '/dist'));
app.use('/public', express.static(__dirname + '/public'));

app.listen(process.env.PORT || 4000, function () {
    console.log('Your node js server is running');
});

process.on('SIGINT', function () {
    db.close(function () {
        console.log('Mongoose disconnected on app termination');
        process.exit(0);
    });
});
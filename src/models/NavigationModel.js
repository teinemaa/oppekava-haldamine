import {action, observable} from "mobx";


export default class NavigationModel {
    @observable sections = {
        subSections: [{
            id: 0,
            title: 'Alushariduse riiklik õppekava'
        }, {
            id: 1,
            title: 'Põhikooli riiklik õppekava',
            subSections: [
                {
                    id: 10,
                    title: 'Õppekava üldosa',
                    subSections: [
                        {
                            id: 101,
                            title: 'Kool'
                        },
                        {
                            id: 102,
                            title: 'Õppekorraldus'
                        },
                        {
                            id: 103,
                            title: 'Õppekeskkond'
                        },
                        {
                            id: 104,
                            title: 'Õpilaste toetamine'
                        },
                        {
                            id: 105,
                            title: 'Karjääriõpe'
                        },
                        {
                            id: 106,
                            title: 'Pere kaasamine'
                        },
                        {
                            id: 107,
                            title: 'Koolipersonali koostöö'
                        },
                        {
                            id: 108,
                            title: 'Loovtöö'
                        },
                        {
                            id: 109,
                            title: 'Uuendamise kord'
                        }

                    ]
                },
                {
                    id: 11,
                    title: 'Keel ja kirjandus',
                    subSections: [
                        {
                            id: 110,
                            title: 'Valdkonna üldosa'
                        },
                        {
                            id: 111,
                            title: 'Eesti keel',
                            subSections: [{
                                id: 1111,
                                title: 'I kooliaste'
                            }, {
                                id: 1112,
                                title: 'II kooliaste'
                            }, {
                                id: 1113,
                                title: 'III kooliaste'
                            }]
                        }, {
                            id: 112,
                            title: 'Kirjandus',
                            subSections: [
                                {
                                    id: 1121,
                                    title: 'II kooliaste'
                                }, {
                                    id: 1122,
                                    title: 'III kooliaste'
                                }
                            ]
                        }, {
                            id: 113,
                            title: 'Vene keel'
                        }, {
                            id: 114,
                            title: 'Kirjandus (vene õppekeelega koolis)'
                        }]
                }, {
                    id: 12,
                    title: 'Võõrkeeled'
                }, {
                    id: 13,
                    title: 'Matemaatika'
                }, {
                    id: 14,
                    title: 'Loodusained'
                }, {
                    id: 15,
                    title: 'Sotsiaalained'
                }, {
                    id: 16,
                    title: 'Kunstiained'
                }, {
                    id: 17,
                    title: 'Tehnoloogia'
                }, {
                    id: 18,
                    title: 'Kehaline kasvatus'
                }]
        }, {
            id: 2,
            title: 'Põhikooli lihtsustatud riiklik õppekava'
        }, {
            id: 3,
            title: 'Gümnaasiumi riiklik õppekava'
        }]
    };

    @observable levels = [{
        level: 1,
        title: 'Riiklik õppekava'
    }, {
        level: 2,
        title: 'Ainevaldkonnad'
    }, {
        level: 3,
        title: 'Valdkonnakava'
    }, {
        level: 4,
        title: 'Ainekava'
    }];

    @observable selected = [0];

    @action
    setSelected(level, sectionId) {
        while (this.selected.length > level) {
            this.selected.pop();
        }
        this.selected.push(sectionId);

        let searchTree = function (element, matchingId) {
            if (element.id == matchingId) {
                return element;
            } else if (element.subSections != null) {
                var i;
                var result = null;
                for (i = 0; result == null && i < element.subSections.length; i++) {
                    result = searchTree(element.subSections[i], matchingId);
                }
                return result;
            }
            return null;
        };

        let selectedElement = searchTree(this.sections, sectionId);

        if (selectedElement && selectedElement.subSections && selectedElement.subSections.length) {
            let subSection = selectedElement.subSections[0];
            this.setSelected(level + 1, subSection.id);
        }
    }

    getSelectedId() {
        return this.selected[this.selected.length - 1];
    }

    isSelected(level, sectionId) {
        return this.selected.length > level && this.selected[level] === sectionId;
    }
}


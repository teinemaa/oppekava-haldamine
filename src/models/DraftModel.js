const axios = require('axios');

export default class DraftModel {

    save(id, draft) {
        id = id ? id : 1;
        return axios.post('/api/drafts?id=' + id, {contents: draft})
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    get(id) {
        id = id ? id : 1;
        return axios.get('/api/drafts?id=' + id)
            .then(function (response) {
                console.log(response);
                return response.data ? response.data[0].contents : null;
            })
            .catch(function (error) {
                console.log(error);
            });
    }

}

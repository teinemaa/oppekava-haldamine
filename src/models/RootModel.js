import GuideModel from "./GuideModel";
import NavigationModel from "./NavigationModel";
import DraftModel from "./DraftModel";

export default class RootStore {
  guide = new GuideModel();
  draft = new DraftModel();
  navigation = new NavigationModel();
}

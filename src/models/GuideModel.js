import {observable} from "mobx";


export default class GuideModel {
    @observable guides = {
        "101": {
            comment: "Põhikooli RÕK üldosa VÄÄRTUSED, KOOLI ERIPÄRA, ÕPPE- JA KASVATUSEEESMÄRGID",
            type: "CURRICULUM",
            description:"Vt PRÕK § 2.  Põhihariduse alusväärtused; PRÕK § 3.  Põhikooli sihiseade; PGS § 3.  Üldhariduskooli alusväärtused; § 4.  Põhikooli ülesanne.",
            title: "Kool",
            drafts: ["Väärtused", "Kooli eripära", "Kooli õppe- ja kasvatuseesmärgid"]
        },
        "102": {
            comment: "Põhikooli RÕK üldosa ÕPPEKORRALDUS",
            type: "CURRICULUM",
            description:"§ 15.  Õppe ja kasvatuse korralduse alused, PGS § 25, lg 4.",
            tags: [
                {
                    title: "Üldpädevused",
                    values: this.guideTags
                },
                {
                    title: "Läbivad teemad",
                    values: ["Elukestev õpe ja karjääri planeerimine.", "Keskkond ja jätkusuutlik areng.", "Kodanikualgatus ja ettevõtlikkus.", "Kultuuriline identiteet.", "Teabekeskkond", "Väärtused ja kõlblus.", "Kultuuriline identiteet.", "Tehnoloogia ja innovatsioon."],
                }
            ],
            drafts: ["Tunnijaotusplaan (tabeli loomine)", "Üldpädevuste kujunemise toetamine", "Läbivate teemade rakendamine", "Lõimingu rakendamine"]
        },
        "110": {
            comment: "Eesti keele ja kirjanduse  VALDKONNA üldosa",
            type: "CURRICULUM",
            title: "Lõimingu võimalused",
            description:"Kirjelda, kuidas ainevaldkonnas kujundatakse üldpädevusi, millised on rõhuasetused. Millised on lõimingu võimalused valdkonnasiseselt ja valdkonnaüleselt.",
            tags: [
                {
                    title: "Üldpädevused",
                    values: this.guideTags
                }
            ],
            drafts: ["Üldpädevuste kujundamine ainevaldkonnas", "Lõimingu võimalused"]
        },
        "1111": {
            comment: "EESTI KEEL I kooliaste",
            type: "SUBJECT",
            title: "Osaoskused",
            description:"Kirjelda klassiti, millised õpitulemused saavutatakse ja milliseid õppematerjale ja -meetodeid kasutatakse, lähtudes kooli väärtustest ja eripärast.",
            studyOutcomeTitle: "Õpitulemused",
            studyContentTitle: "Õppesisu",
            drafts: ["I klass", "II klass", "III klass"],
            skills: [
                {
                    name: "Lugemisoskus",
                    outcomes: [
                        "õpitulemus 1"
                    ],
                    content: "õppesisu 1"
                },
                {
                    name: "Tähestiku tundime",
                    outcomes: [
                        "õpitulemus 1"
                    ],
                    content: "õppesisu 1"
                }
            ],
        },
        "1112": {
            comment: "EESTI KEEL II kooliaste",
            type: "SUBJECT",
            title: "Osaoskused",
            description:"Kirjelda klassiti, millised õpitulemused saavutatakse ja milliseid õppematerjale ja -meetodeid kasutatakse, lähtudes kooli väärtustest ja eripärast.",
            studyOutcomeTitle: "Õpitulemused",
            studyContentTitle: "Õppesisu",
            drafts: ["IV klass", "V klass", "VI klass"],
            skills: [
                {
                    name: "Suuline ja kirjalik suhtlus",
                    outcomes: [
                        "oskab juhendamise abil olukorrale vastava suhtluskanali valida",
                        "esitab kuuldu ja loetu kohta küsimusi ning avaldab arvamust",
                        "lahendab lihtsamaid probleemülesandeid paaris- ja rühmatöös",
                        "oskab oma seisukohta põhjendada ja vajaduse korral sellest taganeda",
                        "oskab teha kuuldu ja loetu kohta nii suulist kui ka kirjalikku kokkuvõtet"
                    ],
                    content: "Klassivestlus, diskussioon. Telefonivestluse alustamine ja lõpetamine. Virtuaalkeskkonnas suhtlemise eesmärgid, võimalused, ohud, privaatsus ja avalikkus. E-kiri.\n" +
                        "Suuline arvamusavaldus ja vestlus etteantud teemal, vastulausele reageerimine ning seisukohast loobumine. Väite põhjendamine.\n" +
                        "Seisukoha põhjendamine paaris- või rühmatöö ajal ning kõnejärg. Kaaslase täiendamine ja parandamine.\n" +
                        "Kaasõpilaste töödele hinnangu andmine ja tunnustuse avaldamine."
                },
                {
                    name: "Teksti vastuvõtt",
                    outcomes: [
                        "tunneb õpitud tekstiliike ja nende kasutamise võimalusi",
                        "loeb ja mõistab eakohaseid õpi- ja elutarbelisi ning huvivaldkondade tekste",
                        "võrdleb tekste, esitab küsimusi ja avaldab arvamust, teeb tekstist lühikokkuvõtte",
                        "kasutab töös tekstidega õpitud keele- ja tekstimõisteid"
                    ],
                    content: "Põhimõisted: kokkuvõte, konspekt, foto, karikatuur, reklaam, tarbetekst, põhjus, tagajärg.\n" +
                        "Reklaam: sõnum, pildi ja sõna mõju reklaamis, adressaat, lastele mõeldud reklaam. \n" +
                        "Tarbetekstide keel: kataloogid, kasutusjuhendid, toodete etiketid.\n" +
                        " Tarbe- ja õppetekstide (reegel, juhend, tabel, skeem, kaart, saatekava jne) mõtestatud lugemine. Trükised (raamat, ajaleht, ajakiri), nendes orienteerumine ning vajaliku teabe leidmine. Visuaalselt esitatud info (foto, joonise, graafiku) põhjal lihtsamate järelduste tegemine, seoste leidmine.\n" +
                        "Skeemist, tabelist, kuulutusest, sõiduplaanist ja hinnakirjast andmete kirjapanek ning seoste esiletoomine.\n" +
                        "Eesmärgistatud lugemine, kuulamine, vaatamine, kokkuvõtte tegemine, kuuldu konspekteerimine. Eesmärgistatud lugemine, kuulamine, vaatamine, kokkuvõtte tegemine, kuuldu konspekteerimine."
                },
                {
                    name: "Tekstiloome",
                    outcomes: [
                        "leiab juhendamise abil tekstiloomeks vajalikku teavet",
                        "koostab ning peab lühikese ettekande",
                        "tunneb kirjutamiseks ja esinemiseks valmistumise põhietappe",
                        "jutustab, kirjeldab ning arutleb suuliselt ja kirjalikult",
                        "esineb suuliselt, võtab sõna, koostab ja peab lühikese ettekande",
                        "avaldab viisakalt ja olukorrast lähtudes arvamust sündmuse, nähtuse või teksti kohta nii suuliselt kui ka kirjalikult",
                        "kirjutab eesmärgipäraselt loovtöid ja kirju",
                        "vormistab kirjalikud tekstid korrektselt",
                        "kasutab tekste luues ja seostades omandatud keele- ja tekstimõisteid",
                    ],
                    content: "Protsesskirjutamine: suuline eeltöö, kava ja mõttekaardi koostamine, teksti kirjutamine, viimistlemine, toimetamine ja avaldamine, tagasiside saamine. \n" +
                        "Kirjandi ülesehitus. Sissejuhatus, teemaarendus, lõpetus. Mustand. Oma vigade leidmine ja parandamine. Lisateabe otsimine. Alustekst, selle edasiarenduse lihtsamad võtted. \n" +
                        "Visuaalsed ja tekstilised infoallikad ning nende usaldusväärsus. Eri allikatest pärit info võrdlemine, olulise eristamine ebaolulisest. Teksti loomine pildi- ja näitmaterjali põhjal. Fakt ja arvamus. Uue info seostamine oma teadmiste ja kogemustega.\n" +
                        "Lühiettekanne ja esitlus internetist või teatmeteostest leitud info põhjal. Privaatses ja avalikus keskkonnas suhtlemise eetika. \n" +
                        "Kiri, ümbriku vormistamine."
                },
                {
                    name: "Õigekeelsus ja keelehoole",
                    outcomes: [
                        "tunneb eesti keele häälikusüsteemi, sõnaliikide tüüpjuhte ja lihtvormide kasutust ning järgib eesti õigekirja aluseid ja õpitud põhireegleid",
                        "oskab kasutada veebi- ja pabersõnaraamatut sõna tähenduse ja õigekirja kontrollimiseks",
                        "tunneb kirjutamiseks ja esinemiseks valmistumise põhietappe",
                        "rakendab omandatud keeleteadmisi tekstiloomes ning tekste analüüsides ja hinnates",
                    ],
                    content: "Kirjakeel, argikeel ja murdekeel\n" +
                        "Häälikuõpetus ja õigekiri\n" +
                        "g, b, d s-i kõrval. Tähe h õigekiri. Tähtede i ja j õigekiri (tegijanimed ja liitsõnad). Liidete -gi ja -ki õigekiri. Sulghäälik võõrsõna algul ja sõna lõpus, sulghäälik sõna keskel. \n" +
                        "Tähtede f ja š õigekiri. Sagedamini esinevate võõrsõnade tähendus, hääldus ja õigekiri. Silbitamine ja poolitamine (ka liitsõnades).\n" +
                        "Õigekirja kontrollimine sõnaraamatutest (nii raamatu- kui ka veebivariandist). \n" +
                        "Sõnavaraõpetus\n" +
                        "Kirjakeelne ja argikeelne sõnavara, uudissõnad, murdesõnad, släng. Sünonüümid,\n" +
                        "antonüümid, homonüümid, nende kasutamine. Sõnavaliku täpsus erinevates tekstides.\n" +
                        "Igapäevaste võõrsõnade asendamine omasõnadega ja vastupidi.\n" +
                        "Sõna tähenduse leidmine sõnaraamatutest (nii raamatu- kui ka veebivariandist).\n" +
                        "Vormiõpetus\n" +
                        "Tegusõna. Tegusõna ajad: olevik, liht-, täis- ja enneminevik. Nimisõnade kokku- ja lahkukirjutamine. Omadussõna käänamine koos nimisõnaga. lik- ja ne-liiteliste omadussõnade käänamine ning õigekiri. Omadussõnade võrdlusastmed. Võrdlusastmete kasutamine. Omadussõnade tuletusliited. Omadussõnade kokku- ja lahkukirjutamine (ne- ja line-liitelised omadussõnad). \n" +
                        "Arvsõnade õigekiri. Rooma numbrite kirjutamine. Põhi- ja järgarvsõnade kokku- ja lahkukirjutamine. Kuupäeva kirjutamise võimalusi. Arvsõnade käänamine. Põhi- ja järgarvsõnade kirjutamine sõnade ja numbritega, nende lugemine. Arvsõnade kasutamine tekstis.\n" +
                        "Sõnaraamatute kasutamine käändsõna põhivormide kontrollimiseks.\n" +
                        "Lauseõpetus\n" +
                        "Liitlause. Lihtlausete sidumine liitlauseks. Sidesõnaga ja sidesõnata liitlause. Kahe järjestikuse osalausega liitlause kirjavahemärgistamine. \n" +
                        "Otsekõne ja saatelause. Saatelause otsekõne ees, keskel ja järel. \n" +
                        "Otsekõne kirjavahemärgid. Otsekõne kasutamise võimalusi. \n" +
                        "Üte ja selle kirjavahemärgid. \n" +
                        "Muud õigekirjateemad\n" +
                        "Algustäheõigekiri: ajaloosündmused; ametinimetused ja üldnimetused; perioodikaväljaanded; teoste pealkirjad.\n" +
                        "Üldkasutatavad lühendid. Lühendite õigekiri. Lühendite lugemine."
                },

            ],
        },
        "1113": {
            comment: "EESTI KEEL III kooliaste",
            type: "SUBJECT",
            title: "Osaoskused",
            description:"Kirjelda klassiti, millised õpitulemused saavutatakse ja milliseid õppematerjale ja -meetodeid kasutatakse, lähtudes kooli väärtustest ja eripärast.",
            studyOutcomeTitle: "Õpitulemused",
            studyContentTitle: "Õppesisu",
            drafts: ["VII klass", "VIII klass", "IX klass"],
            skills: [
                {
                    name: "Perfektne õigekiri",
                    outcomes: [
                        "õpitulemus 1"
                    ],
                    content: "õppesisu 1"
                }
            ],
        },
        "1121": {
            comment: "KIRJANDUS",
            type: "SUBJECT",
            title: "Osaoskused",
            description:"Kirjelda klassiti, millised õpitulemused saavutatakse ja milliseid õppematerjale ja -meetodeid kasutatakse, lähtudes kooli väärtustest ja eripärast. Kirjelda tekstide valiku põhimõtteid.",
            studyOutcomeTitle: "Õpitulemused",
            studyContentTitle: "Õppesisu",
            drafts: ["V klass", "VI klass"],
            skills: [
                {
                    name: "Lugemine",
                    outcomes: [
                        "on lugenud läbi vähemalt kaheksa eakohast eri žanris väärtkirjandusteost",
                        "loeb ladusalt ja ilmekalt",
                        "väärtustab ilukirjanduse lugemist",
                        "oskab tutvustada loetud kirjandusteose autorit, sisu ja tegelasi ning kõnelda oma lugemiselamustest ja -kogemustest"
                    ],
                    content: "Lugemise eesmärgistamine. Lugemiseks valmistumine, keskendunud lugemine. Lugemistehnika arendamine, häälega ja hääleta lugemine, pauside, tempo ja intonatsiooni jälgimine.\n" +
                        "Eesmärgistatud ülelugemine. Oma lugemise jälgimine ning lugemisoskuse hindamine. Etteloetava teksti eesmärgistatud jälgimine.\n" +
                        "Huvipakkuva kirjandusteose leidmine ja iseseisev lugemine. Lugemisrõõm. Loetud raamatu autori, sisu ja tegelaste tutvustamine klassikaaslastele.\n" +
                        "Lugemissoovituste jagamine klassikaaslastele. Soovitatud tervikteoste kodulugemine ning ühisaruteluks vajalike ülesannete täitmine."
                },
                {
                    name: "Jutustamine",
                    outcomes: [
                        "jutustab tekstilähedaselt kavapunktide järgi või märksõnade abil",
                        "jutustab mõttelt sidusa tervikliku ülesehitusega selgelt sõnastatud loo, tuginedes kirjanduslikule tekstile, tõsielusündmusele või oma fantaasiale",
                        "jutustab piltteksti põhjal"
                    ],
                    content: "Tekstilähedane sündmustest jutustamine kavapunktide järgi. Tekstilähedane jutustamine märksõnade abil. Aheljutustamine.\n" +
                        "Loo ümberjutustamine uute tegelaste ja sündmuste lisamisega. Iseendaga või kellegi teisega toimunud sündmusest või mälestuspildist jutustamine.eklaam: sõnum, pildi ja sõna mõju reklaamis, adressaat, lastele mõeldud reklaam.Loo ümberjutustamine uute tegelaste ja sündmuste lisamisega. Iseendaga või kellegi teisega toimunud sündmusest või mälestuspildist jutustamine.\n" +
                        "Tarbetekstide keel: kataloogid, kasutusjuhendid, toodete etiketid.\n" +
                        "Jutustamine piltteksti (foto, illustratsioon, karikatuur, koomiks) põhjal. Fantaasialoo jutustamine."
                },
                {
                    name: "Teksti tõlgendamine, analüüs ja mõistmine",
                    outcomes: [
                        "koostab teksti kohta eri liiki küsimusi",
                        "vastab teksti põhjal koostatud küsimustele oma sõnade või tekstinäitega",
                        "koostab teksti kohta sisukava, kasutades küsimusi, väiteid või märksõnu",
                        "järjestab teksti põhjal sündmused ning määrab nende toimumise aja ja koha",
                        "kirjeldab loetud tekstile tuginedes tegelaste välimust, iseloomu ja käitumist, analüüsib nende suhteid, hindab nende käitumist, lähtudes üldtunnustatud moraalinormidest, ning võrdleb ennast mõne tegelasega",
                        "leiab lõigu kesksed mõtted ja sõnastab peamõtte",
                        "arutleb kirjandusliku tervikteksti või katkendi põhjal teksti teema, põhisündmuste, tegelaste, nende probleemide ja väärtushoiakute üle, avaldab ja põhjendab oma arvamust, valides sobivaid näiteid nii tekstist kui ka igapäevaelust",
                        "otsib teavet tundmatute sõnade kohta ning teeb endale selgeks nende tähenduse",
                        "tunneb ära ja kasutab tekstides tuntumaid kõnekujundeid",
                        "seletab õpitud vanasõnade ja kõnekäändude tähendust",
                        "mõtestab luuletuse tähendust, tuginedes enda elamustele ja kogemustele",
                        "seletab õpitud kõnekujundeid, rahvalaulu, muistendi ja muinasjutu olemust oma sõnadega",
                    ],
                    content: "Teksti kui terviku mõistmine. Mälu- ehk faktiküsimuste ja loovküsimuste koostamine. Küsimustele vastamine tsitaadiga (tekstilõigu või fraasiga), teksti järgi oma sõnadega ning peast. Teksti kavastamine: kavapunktid küsi- ja väitlausete ning märksõnadena.\n" +
                        "Lõikude kesksete mõtete otsimine ja peamõtte sõnastamine. Teksti teema ja peamõtte sõnastamine. Arutlemine mõne teoses käsitletud teema üle. Oma arvamuse sõnastamine ja põhjendamine. Illustratiivsete näidete (nt tsitaadid, iseloomulikud detailid) otsimine tekstist.\n" +
                        "Tegelaste probleemi leidmine ja sõnastamine. Teose sündmustiku ning tegelaste suhestamine (nt võrdlemine) enda ja ümbritsevaga. Pea- ja kõrvaltegelaste leidmine, tegelase muutumise ja tegelastevaheliste suhete jälgimine, tegelaste iseloomustamine ja nende käitumise põhjendamine.\n" +
                        "Kujundliku mõtlemise ja keelekasutuse, sh metakeele mõistmine. Mõisted: epiteet, võrdlus, vanasõna, kõnekäänd, muinasjutt, muistend, algriim.\n" +
                        "Riimide leidmine ja loomine. Luuletuse rütmi ja kõla tunnetamine. Algriimi leidmine rahvalauludest. Algriimi kasutamine oma tekstis. Luuleteksti tõlgendamine. Oma kujundliku väljendusoskuse hindamine ja arendamine."
                },
                {
                    name: "Teksti esitus ja omalooming",
                    outcomes: [
                        "kirjutab erineva pikkusega erižanrilisi omaloomingulisi töid, sealhulgas kirjeldavas ja jutustavas vormis",
                        "esitab peast luuletuse, lühikese proosa- või rolliteksti, taotledes esituse ladusust ja selgust ning tekstitäpsust",
                    ],
                    content: "Esitamise eesmärgistamine (miks, kellele ja mida). Esitamiseks kohase sõnavara, tempo, hääletugevuse ja intonatsiooni valimine. Õige hingamine ning kehahoid. Luuleteksti esitamine peast. Lühikese proosateksti esitamine (dialoogi või monoloogina). Rollimäng, rolliteksti esitamine.\n" +
                        "Õpilased kirjutavad lühemaid ja pikemaid omaloomingulisi töid.\n" +
                        "Kirjandustekstide valik. II kooliastmes käsitletavad tekstid ja terviklikult loetavad kirjandusteosed valitakse arvestusega, et esindatud oleksid kõik järgmised teemavaldkonnad: väärtused ja kõlblus; kodus ja koolis; omakultuur ja kultuuriline mitmekesisus; mängiv inimene; keskkond ja ühiskonna jätkusuutlik areng; kodanikuühiskond ja rahvussuhted; teabekeskkond, tehnoloogia ja innovatsioon."
                },

            ],
        },
    };

    guideTags = [
        {
            title: "Kultuuri- ja väärtuspädevus",
            descripition: "Suutlikkus hinnata inimsuhteid ja tegevusi üldkehtivate moraalinormide seisukohast; tajuda ja väärtustada oma seotust teiste inimestega, ühiskonnaga, loodusega, oma ja teiste maade ja rahvaste kultuuripärandiga ning nüüdiskultuuri sündmustega; väärtustada loomingut ja kujundada ilumeelt; hinnata üldinimlikke ja ühiskondlikke väärtusi, väärtustada inimlikku, kultuurilist ja looduslikku mitmekesisust; teadvustada oma väärtushinnanguid."
        },
        {
            title: "Sotsiaalne ja kodanikupädevus.",
            descripition: "Suutlikkus ennast teostada; toimida aktiivse, teadliku, abivalmi ja vastutustundliku kodanikuna ning toetada ühiskonna demokraatlikku arengut; teada ja järgida ühiskondlikke väärtusi ja norme; austada erinevate keskkondade reegleid ja ühiskondlikku mitmekesisust, religioonide ja rahvuste omapära; teha koostööd teiste inimestega erinevates situatsioonides; aktsepteerida inimeste ja nende väärtushinnangute erinevusi ning arvestada neid suhtlemisel."
        },
        {
            title: "Enesemääratluspädevus",
            descripition: "Suutlikkus mõista ja hinnata iseennast, oma nõrku ja tugevaid külgi; analüüsida oma käitumist erinevates olukordades; käituda ohutult ja järgida tervislikke eluviise; lahendada suhtlemisprobleeme."
        },
        {
            title: "Õpipädevus",
            descripition: "Suutlikkus organiseerida õppekeskkonda individuaalselt ja rühmas ning hankida õppimiseks, hobideks, tervisekäitumiseks ja karjäärivalikuteks vajaminevat teavet; planeerida õppimist ja seda plaani järgida; kasutada õpitut erinevates olukordades ja probleeme lahendades; seostada omandatud teadmisi varemõpituga; analüüsida oma teadmisi ja oskusi, motiveeritust ja enesekindlust ning selle põhjal edasise õppimise vajadusi."
        },
        {
            title: "Suhtluspädevus",
            descripition: "Suutlikkus ennast selgelt, asjakohaselt ja viisakalt väljendada nii emakeeles kui ka võõrkeeltes, arvestades olukordi ja mõistes suhtluspartnereid ning suhtlemise turvalisust; ennast esitleda, oma seisukohti esitada ja põhjendada; lugeda ning eristada ja mõista teabe- ja tarbetekste ning ilukirjandust; kirjutada eri liiki tekste, kasutades korrektset viitamist, kohaseid keelevahendeid ja sobivat stiili; väärtustada õigekeelsust ja väljendusrikast keelt ning kokkuleppel põhinevat suhtlemisviisi."
        },
        {
            title: "Matemaatika-, loodusteaduste ja tehnoloogiaalane pädevus",
            descripition: "Suutlikkus kasutada matemaatikale omast keelt, sümboleid, meetodeid koolis ja igapäevaelus; suutlikkus kirjeldada ümbritsevat maailma loodusteaduslike mudelite ja mõõtmisvahendite abil ning teha tõenduspõhiseid otsuseid; mõista loodusteaduste ja tehnoloogia olulisust ja piiranguid; kasutada uusi tehnoloogiaid eesmärgipäraselt."
        },
        {
            title: "Ettevõtlikkuspädevus",
            descripition: "Suutlikkus ideid luua ja ellu viia, kasutades omandatud teadmisi ja oskusi erinevates elu- ja tegevusvaldkondades; näha probleeme ja neis peituvaid võimalusi, aidata kaasa probleemide lahendamisele; seada eesmärke, koostada plaane, neid tutvustada ja ellu viia; korraldada ühistegevusi ja neist osa võtta, näidata algatusvõimet ja vastutada tulemuste eest; reageerida loovalt, uuendusmeelselt ja paindlikult muutustele; võtta arukaid riske."
        },
        {
            title: "Digipädevus",
            descripition: "Suutlikkus kasutada uuenevat digitehnoloogiat toimetulekuks kiiresti muutuvas ühiskonnas nii õppimisel, kodanikuna tegutsedes kui ka kogukondades suheldes; leida ja säilitada digivahendite abil infot ning hinnata selle asjakohasust ja usaldusväärsust; osaleda digitaalses sisuloomes, sh tekstide, piltide, multimeediumide loomisel ja kasutamisel; kasutada probleemilahenduseks sobivaid digivahendeid ja võtteid, suhelda ja teha koostööd erinevates digikeskkondades; olla teadlik digikeskkonna ohtudest ning osata kaitsta oma privaatsust, isikuandmeid ja digitaalset identiteeti; järgida digikeskkonnas samu moraali- ja väärtuspõhimõtteid nagu igapäevaelus."
        }]
    ;
}

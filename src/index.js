require( 'bootstrap/dist/css/bootstrap.min.css');
require( './okava.css');


import React from "react";
import {render} from "react-dom";

import CurriculumWorksheet from "./components/CurriculumWorksheet";
import RootStore from "./models/RootModel";
import {Provider} from "mobx-react";

const store = new RootStore();

render(
  <div>
    <Provider {...store}>
        <CurriculumWorksheet/>
    </Provider>
  </div>,
  document.getElementById("root")
);

// playing around in the console
window.store = store;

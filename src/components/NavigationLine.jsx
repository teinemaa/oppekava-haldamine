import React from "react";
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

class NavigationContainer extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <CKEditor
                editor={this.state.editor}
                data={this.props.value}
                onInit={editor => {
                    // You can store the "editor" and use when it is needed.
                    console.log('Editor is ready to use!', editor);
                }}
                onChange={(event, editor) => {
                    const data = editor.getData();
                    console.log({event, editor, data});
                }}
            />
        )
    }

}

export default NavigationContainer;

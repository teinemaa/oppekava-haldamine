import React from "react";
import {observer} from "mobx-react";
import ClassicEditorContainer from "./ClassicEditorContainer";
import {Card, CardBody, CardTitle, Col} from 'reactstrap';


const Draft = observer(({guide}) => (
    <Col xs="6" sm="6"  className="pt-3" >
        <h2>Meie kooli õppekava</h2>
        {guide.drafts.map(draft => (
            <Card className="mt-3">
                <CardBody>
                    <CardTitle>
                        {draft}
                    </CardTitle>
                    <ClassicEditorContainer entityId={draft}></ClassicEditorContainer>
                </CardBody>
            </Card>

        ))}
    </Col>
));

export default Draft;

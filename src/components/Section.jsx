import React from "react";
import {observer} from "mobx-react";
import {Row} from 'reactstrap';
import Draft from "./Draft";
import Guide from "./Guide";


const Section = observer(({ guide }) => (

        <Row className="">
            <Guide guide={guide}/>
            <Draft guide={guide}/>
        </Row>
));

export default Section;

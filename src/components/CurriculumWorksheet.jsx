import React from "react";
import {inject, observer} from "mobx-react";
import Section from "./Section";
import NavigationContainer from "./NavigationContainer";
import Container from "reactstrap/es/Container";

var imageExists = require('image-exists');


@inject("guide", "navigation")
@observer

class CurriculumWorksheet extends React.Component {
  
  render() {

    var imagePath = '/public/';
    imageExists(imagePath+'innove_logo1.jpg',function(exists){if(exists){}else{imagePath='./';}});
      
    const selected = this.props.navigation.getSelectedId();
      const guide = this.props.guide.guides[''+selected];

      if (this.props.navigation.selected.length === 1) {
          return (
              <Container className="h-100 d-flex  flex-wrap pb-5">

                  <div className="align-self-end w-100" align="center">
                      <img height="70"  src={imagePath+'innove_logo1.jpg'}/>
                      <img height="30" className="ml-4" src={imagePath+'./hitsa-logo-est.png'}/>
                      <img height="35" className="ml-5" src={imagePath+'./progmatic-logo.png'}/>
                      <h1 style={{ fontFamily: 'Roboto'}} className="display-2 mt-5">Õppekavataja</h1></div>
                  <div className="align-self-end"><NavigationContainer/></div>

              </Container>
          )
      }



      return <Container>
          <NavigationContainer/>

          {

            guide ? <Section guide={guide} /> : null
            
            
          }

      </Container>;
  }

}

export default CurriculumWorksheet;

import React from "react";
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {inject} from "mobx-react";

@inject('draft')
class ClassicEditorContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            entityId: this.props.entityId,
            editor: null,
            editorConfig: {
                toolbar: [['Bold'], ['Italic'], ['bulletedList'], ['inserttable'], ['Link']],
                height: '500'
            }
        };
    }
   //
    render() {
        if (this.state.entityId != this.props.entityId && this.state.editor) {
            this.state.editor.setData('');
            this.setState({
                entityId: this.props.entityId
            });
            this.props.draft.get(this.props.entityId).then((data) => {
                if (data)
                    this.state.editor.setData(data);
                else
                    this.state.editor.setData(data);
            });
        }
        return (
            <CKEditor
                id={this.props.entityId.length}
                editor={ClassicEditor}
                config={this.state.editorConfig}
                onInit={editor => {
                    this.setState({editor: editor});
                    this.props.draft.get(this.props.entityId).then((data) => {
                        if (data)
                            editor.setData(data);
                        else
                            editor.setData('');
                    });
                    // You can store the "editor" and use when it is needed.
                    console.log('Editor is ready to use!', editor);
                }}
                onBlur={(event, editor) => {
                    const data = editor.getData();
                    this.props.draft.save(this.props.entityId, data).then(() => {
                        console.log('saved');
                    });
                    console.log({event, editor, data});
                }}
            />
        )
    }

}

export default ClassicEditorContainer;

import React from 'react';
import classNames from 'classnames';
import { tagPropType } from 'reactstrap';
export function mapToCssModules(className = '', cssModule) {
  if (!cssModule) return className;
  return className
      .split(' ')
      .map(c => cssModule[c] || c)
      .join(' ');
}
const defaultProps = {
  color: 'secondary',
  tag: 'span',
};

class ButtonBadge extends React.Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    if (this.props.disabled) {
      e.preventDefault();
      return;
    }

    if (this.props.onClick) {
      this.props.onClick(e);
    }
  }

  render() {
    let {
      active,
      'aria-label': ariaLabel,
      block,
      className,
      close,
      cssModule,
      color,
      outline,
      size,
      tag: Tag,
      innerRef,
      ...attributes
    } = this.props;

    if (close && typeof attributes.children === 'undefined') {
      attributes.children = <span aria-hidden>×</span>;
    }

    const btnOutlineColor = `badge${outline ? '-outline' : ''}-${color}`;

    const classes = mapToCssModules(classNames(
        className,
        { close },
        close || 'badge',
        close || btnOutlineColor,
        size ? `badge-${size}` : false,
        block ? 'badge  -block' : false,
        { active, disabled: this.props.disabled }
    ), cssModule);

    if (attributes.href && Tag === 'button') {
      Tag = 'a';
    }

    const defaultAriaLabel = close ? 'Close' : null;

    return (
        <Tag
            type={(Tag === 'button' && attributes.onClick) ? 'button' : undefined}
            {...attributes}
            className={classes}
            ref={innerRef}
            onClick={this.onClick}
            aria-label={ariaLabel || defaultAriaLabel}
        />
    );
  }
}

ButtonBadge.defaultProps = defaultProps;

export default ButtonBadge;

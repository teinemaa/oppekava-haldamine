import React from "react";
import {observer} from "mobx-react";
import {
    Badge,
    Card,
    CardBody,
    Col,
    ListGroup,
    ListGroupItem,
    PopoverBody,
    PopoverHeader,
    UncontrolledCollapse,
    UncontrolledPopover
} from 'reactstrap';
import ButtonBadge from "./ButtonBadge";

const Guide = observer(({guide}) => {
        if (guide.type === "SUBJECT") {
            return (
                <Col xs="6" sm="6" className="pt-3" style={{backgroundColor: "#f8f8f8"}}>
                    <h2>Riikliku õppekava nõuded</h2>
                    {guide.description ? <p>{guide.description}</p> : null}
                    <p><h3>{guide.title}</h3></p>
                    <ListGroup>
                        {guide.skills.map((skill, index) => (
                            <React.Fragment>
                                <ListGroupItem tag="a" id={"toggler-" + index} href="#"
                                >
                                    {skill.name}
                                </ListGroupItem>
                                <UncontrolledCollapse toggler={"#toggler-" + index}>
                                    <Card>
                                        <CardBody style={{backgroundColor: "#f8f8f8"}}>
                                            <h6>{guide.studyOutcomeTitle}</h6>
                                            <p>
                                                <ListGroup flush>
                                                    {skill.outcomes.map(outcome =>
                                                        <ListGroupItem tag="a" href="#" onClick={event => event.preventDefault()}>{outcome}<br/></ListGroupItem>
                                                    )}
                                                </ListGroup></p>
                                            <h6>{guide.studyContentTitle}</h6>
                                            <p>
                                                {skill.content}
                                            </p>
                                        </CardBody>
                                    </Card>
                                </UncontrolledCollapse>
                            </React.Fragment>
                        ))}
                    </ListGroup>

                </Col>
            )
        }
        return (
            <Col xs="6" sm="6" className="pt-3" style={{backgroundColor: "#f8f8f8"}}>
                <h2>Riikliku õppekava nõuded</h2>
                {guide.description ? <p>{guide.description}</p> : null}
                {guide.tags && guide.tags.map(tags => (
                    <p><h4>{tags.title}</h4>
                        {tags.values.map((tag, index) => {

                                    if (tag && !tag.title) {
                                        return <Badge color="secondary" className="mr-2 mt-1">{tag}</Badge>
                                    } else if (tag && tag.title) {
                                        let tagId = 't' + index + tagHash(tag.title);
                                        return [<ButtonBadge color="secondary" key={'v' + tagId} id={tagId} className="mr-2 mt-1">{tag.title}</ButtonBadge>,
                                            <UncontrolledPopover  key={'p' + tagId}  placement="bottom" target={tagId} trigger='hover'>
                                                <PopoverHeader>{tag.title}</PopoverHeader>
                                                <PopoverBody>{tag.descripition}</PopoverBody>
                                            </UncontrolledPopover>]
                                    }
                            }
                        )}</p>
                ))}

            </Col>
        );

    })
;

function tagHash(value) {
    var hash = 0, i, chr;
    if (value.length === 0) return hash;
    for (i = 0; i < value.length; i++) {
        chr = value.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

export default Guide;

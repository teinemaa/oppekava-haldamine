import React from "react";
import {inject, observer} from "mobx-react";
import {Button, ButtonGroup, Col, Row} from "reactstrap";
import Container from "reactstrap/es/Container";

@inject("navigation")
@observer
class NavigationContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let tree = this.props.navigation.sections;
        return (
            <Row>
                <Col className="p-2 w-100">
                <Container>
                    {this.renderButtonLevel(tree, 1)}
                </Container>
                </Col>
            </Row>
        );

    }

    renderButtonLevel(tree, level) {
        return [<Row key={'nr' + tree.id}><Col className="pt-2 pb-1"><ButtonGroup key={'2' + tree.id}>{tree.subSections.map((section, index) => <Button color="primary" className="pl-4 pr-4" key={section.id} onClick={() => {
            this.props.navigation.setSelected(level, section.id)
        }} active={this.props.navigation.isSelected(level, section.id)}>{section.title}</Button>)}</ButtonGroup></Col></Row>, tree.subSections.map((section, index) => this.renderSubLevel( section, level))];
    }

    renderSubLevel(section, level) {
        return this.props.navigation.isSelected(level, section.id) && section.subSections ? this.renderButtonLevel(section, level + 1) : null;
    }
}

export default NavigationContainer;
